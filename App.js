import React from 'react';
import MapView, { Marker } from 'react-native-maps';
import {
  Platform,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  View,
  Alert,
  ActivityIndicator,
} from 'react-native';
import * as Device from 'expo-device';
import * as Location from 'expo-location';

const { width, height } = Dimensions.get('window');

const defaultCoordinates = {
  latitude: 56.488678,
  longitude: 84.952039,
};

const defaultDeltas = {
  latitudeDelta: 0.015,
  longitudeDelta: 0.0121,
};

export default function App() {
  const [errorMessage, setErrorMessage] = React.useState(null);
  const [location, setLocation] = React.useState(null);

  React.useEffect(() => {
    (async () => {
      if (Platform.OS === 'android' && !Device.isDevice) {
        setErrorMessage(
          'Geolocation will not work on emulator. Please run application on your device.'
        );
        return;
      }
      const { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMessage('Permission to geolocation was denied.');
        return;
      }

      const location = await Location.getCurrentPositionAsync();
      setLocation(location);
    })();
  }, []);

  const region = React.useMemo(() => {
    if (location !== null)
      return {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        ...defaultDeltas,
      };

    return {
      ...defaultCoordinates,
      ...defaultDeltas,
    };
  }, [location]);

  React.useEffect(() => {
    if (errorMessage !== null) {
      Alert.alert(errorMessage);
    }
  }, [errorMessage]);

  return (
    <View style={styles.container} forceInset={{ top: 'always' }}>
      <MapView style={styles.map} loadingEnabled={true} region={region}>
        {location !== null && (
          <Marker
            coordinate={{
              longitude: location.coords.longitude,
              latitude: location.coords.latitude,
            }}
          />
        )}
      </MapView>
      {location === null && (
        <View style={styles.loading}>
          <View style={styles.loadingOverlay} />
          <ActivityIndicator size="large" />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#32a852',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width,
    height,
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loadingOverlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: '#575757',
    opacity: 0.5,
  },
});
